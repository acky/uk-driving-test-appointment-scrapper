package uk.driving.test.cancellations;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Browser {

	private static final Logger log = LoggerFactory.getLogger(Browser.class);

	static final String DRIVING_TEST_CHANGE_URL = "https://driverpracticaltest.direct.gov.uk/login";
	private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("EEE, dd-MMM-yy");

	private WebDriver driver;

	private final LocalDate currentTestDate;
	private final String drivingLicenseNumber;
	private final String appRefNumber;

	@Autowired
	private Mailer mailer;

	public Browser(@Value("${chrome.driver.path}") String chromeDriverPath,
			@Value("${current.test.date}") String currentDate,
			@Value("${application.reference.number}") String appRefNumber,
			@Value("${driving.licence.number}") String drivingLicenseNumber) {
		System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, chromeDriverPath);
		currentTestDate = LocalDate.parse(currentDate, DATE_TIME_FORMATTER);
		this.drivingLicenseNumber = drivingLicenseNumber;
		this.appRefNumber = appRefNumber;
	}

	@Scheduled(cron = "${cron.schedule}")
	public void findDatesEarlierThanTestDate() {
		try {
			// Start Chrome and minimize
			ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
			// options.addArguments("window-size=1200x600");
			driver = new ChromeDriver(options);
			// driver.manage().window().setPosition(new Point(-2000, 0));
			// open web page
			driver.get(DRIVING_TEST_CHANGE_URL);
			// find and fill license number
			WebElement licenceNumber = driver.findElement(By.id("driving-licence-number"));
			licenceNumber.sendKeys(drivingLicenseNumber);
			// find and fill app ref number
			WebElement referenceNumber = driver.findElement(By.id("application-reference-number"));
			referenceNumber.sendKeys(appRefNumber);
			referenceNumber.submit();
			// find and click change date time
			WebElement changeDateTimeBtn = driver.findElement(By.id("date-time-change"));
			changeDateTimeBtn.click();
			// select earliest available and proceed
			WebElement findEarliestBtn = driver.findElement(By.id("test-choice-earliest"));
			findEarliestBtn.click();
			findEarliestBtn.submit();
			// find all available days from calendar
			List<WebElement> availableDateElements = driver
					.findElements(By.xpath("//td[@class='BookingCalendar-date--bookable ']"));
			// extract date from available calendar days which are earlier
			List<String> availableDates = availableDateElements.stream()
					.map(dateElement -> LocalDate.parse(
							dateElement.findElement(By.xpath(".//a")).getAttribute("data-date"), DATE_TIME_FORMATTER))
					.filter(date -> date.isBefore(currentTestDate)).map(date -> date.format(DATE_FORMATTER))
					.collect(Collectors.toList());
			log.info("Dates found {}\n{}", availableDates.size(),
					availableDates.stream().collect(Collectors.joining("\n")));
			// email dates found
			mailer.email(availableDates, drivingLicenseNumber, appRefNumber);
		} catch (Exception e) {
			log.error("Issue encountered. Details: {}", e);
		} finally {
			// graceful exit
			driver.close();
		}
	}
}
