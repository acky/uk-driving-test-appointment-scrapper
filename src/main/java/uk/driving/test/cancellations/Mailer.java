package uk.driving.test.cancellations;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class Mailer {

	private static final Logger log = LoggerFactory.getLogger(Mailer.class);

	private static final String DATE_TIME_FORMAT_FOR_SUBJECT = "dd-MMM-yy HH:mm";

	private String subject = "available test dates @ ";
	private JavaMailSenderImpl mailSender;

	private final String to;
	private final Map<String, LocalDateTime> datesMailedHistory;

	public Mailer(@Value("${to.email}") String to, @Value("${from.mail.id}") String from,
			@Value("${from.mail.host}") String mailHost, @Value("${from.mail.host.port}") int port,
			@Value("${from.mail.password}") String mailPassword) {
		this.to = to;
		this.datesMailedHistory = new HashMap<String, LocalDateTime>();
		mailSender = new JavaMailSenderImpl();
		mailSender.setHost(mailHost);
		mailSender.setPort(port);

		mailSender.setUsername(from);
		mailSender.setPassword(mailPassword);

		Properties props = mailSender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");
	}

	public void email(List<String> availableDates, String drivingLicenseNumber, String appRefNumber) {
		LocalDateTime now = LocalDateTime.now();
		availableDates = availableDates.stream().filter(date -> checkIfEmailed(now, date)).collect(Collectors.toList());
		if (availableDates.size() > 0) {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(to);
			message.setSubject(
					subject + LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_FOR_SUBJECT)));
			message.setText("Found following dates available before your booking date.\n"
					+ availableDates.stream().collect(Collectors.joining("\n")) + "\nYour License number: "
					+ drivingLicenseNumber + "\nYour Applicaiton Reference number: " + appRefNumber + "\nBook now "
					+ Browser.DRIVING_TEST_CHANGE_URL);
			log.info("Sending email to {} with content\n", to, message.getText());
			mailSender.send(message);
		} else {
			log.info("No dates found earlier than booked date.");
		}
	}

	boolean checkIfEmailed(LocalDateTime now, String date) {
		LocalDateTime informedAt = datesMailedHistory.get(date);
		if (informedAt != null) {
			if (ChronoUnit.MINUTES.between(informedAt, now) > 30) {
				datesMailedHistory.put(date, now);
				return true;
			} else {
				log.info("Already emailed date {} at {} not sending now.", date, informedAt);
				return false;
			}
		} else {
			datesMailedHistory.put(date, now);
			return true;
		}
	}

}
