Running Directly:

Create a folder and download following items in that folder:
* [Chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) - windows version - file chromedriver_win32.zip
* [application.properties](./src/main/resources/application.properties) file
* [test-cancellations-0.0.1-SNAPSHOT.jar]() application
* [java](https://java.com/en/download/) only if not present and add environment variable using these [instructions](https://www.onlinetutorialspoint.com/java8/java-8-how-to-set-java_home-on-windows10.html)

Now do the following:
* Extract chromedriver.exe from the zip file. 
* open properties file in notepad and update it.
* Open command prompt (start > search for cmd) and run application using command:  
```
java -jar test-cancellations-0.0.1-SNAPSHOT.jar
```

For Developers - Running in an IDE:
* Download [Chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) - windows version - file chromedriver_win32.zip
* Checkout project
* Update application.properties and ensure its on classpath.
* Do a maven clean install
* Run application as Spring boot app


